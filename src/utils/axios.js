import axios from "axios";

const service = axios.create({
  // 给每一个接口地址添加一个公有前缀
  baseURL: "http://localhost:3000",
  timeout: 5000,
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    // 在发送请求之前做些什么
    return config;
  },
  (error) => {
    // 对请求错误做些什么
    console.error(error);
    Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    // 对响应数据做点什么
    return response.data;
  },
  (error) => {
    // 对响应错误做点什么
    console.error(error);
    return Promise.reject(error);
  }
);

export default service;
