import './App.css'
import Router from './router'
function App() {
  return (
    <div className='appBox'>
      <Router></Router>
    </div>
  )
}

export default App
