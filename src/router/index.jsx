import React, { Suspense } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import RouterList from "./routes";
import BeforeEach from "./BeforEach";
import RouteLoading from "../component/routerLoading";
const routerView = (routes) => {
  return routes.map((v, i) => {
    return <Route key={i} path={v.path} element={<BeforeEach route={v}></BeforeEach>}></Route>
  })
}

const generatorRoutes = (routes) => {
  return routes.reduce((p, n) => {
    return n.element ? p.concat(n) : p.concat(n.children ? generatorRoutes(n.children) : []);
  }, [])
}

const Router = () => {
  const flatRoutes = generatorRoutes(RouterList);
  return (
    <BrowserRouter>
      <Suspense fallback={<RouteLoading></RouteLoading>}>
        <Routes>
          {routerView(flatRoutes)}
        </Routes>
      </Suspense>
    </BrowserRouter>
  )
}

export default Router;