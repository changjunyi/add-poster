import React from "react";
import { Navigate } from "react-router-dom";
const BeforeEach = ({route}) => {
  document.title = route.title || "后端管理系统";
  // 获取token
  const token = localStorage.getItem("token");
  // 白名单页面 => 免登陆页面
  // 如登录页面， 不需要登录权限
  const whiteList = ["/login","/register"];
  if (!token) {
    if (whiteList.includes(route.path)) {
      return <div>{route.element}</div>;
    } else {
      return <Navigate to="/login"></Navigate>;
    }
  } else {
    if (whiteList.includes(route.path)) {
      return <Navigate to="/"></Navigate>;
    } else {
      return <div>{route.element}</div>;
    }
  }
}

export default BeforeEach;