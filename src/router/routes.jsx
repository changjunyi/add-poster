import { lazy } from "react";
const ClassView = lazy(() => import("../view/classView/index"));
const SelectedView = lazy(() => import("../view/selectedView/index"));
const WorkbenchView = lazy(() => import("../view/workbenchView/index"));
const PosterView = lazy(() => import("../view/posterView/index"));
const FormView = lazy(() => import("../view/formView/index"));
const InteractionView = lazy(() => import("../view/interactionView/index"));
const VideoView = lazy(() => import("../view/videoView/index"));
const LoginView = lazy(() => import("../view/loginView/index"));
const RegisterView = lazy(() => import("../view/registerView/index"));
const WorkClass = lazy(() => import("../view/workClass/index"));
const WorksView = lazy(() => import("../view/worksView/index"));
const MaterialView = lazy(() => import("../view/materialView/index"));
const RecycleView = lazy(() => import("../view/recycleView/index"));
const RouterList = [
  {
    path: "/",
    element: <ClassView><SelectedView /></ClassView>,
    title: "精选推荐"
  },
  {
    path: "/poster",
    element: <ClassView><PosterView /></ClassView>,
    title: "海报"
  },
  {
    path: "/form",
    element: <ClassView><FormView /></ClassView>,
    title: "表单"
  },
  {
    path: "/interaction",
    element: <ClassView><InteractionView /></ClassView>,
    title: "互动"
  },
  {
    path: "/video",
    element: <ClassView><VideoView /></ClassView>,
    title: "视频"
  },
  {
    path: "/workbench",
    element: <WorkClass><WorkbenchView></WorkbenchView></WorkClass>,
    title: "工作台"
  },
  {
    path: "/works",
    element: <WorkClass><WorksView></WorksView></WorkClass>,
    title: "作品管理"
  },
  {
    path: "/material",
    element: <WorkClass><MaterialView></MaterialView></WorkClass>,
    title: "素材管理"
  },
  {
    path: "/recycle",
    element: <WorkClass><RecycleView></RecycleView></WorkClass>,
    title: "回收站"
  },
  {
    path: "/login",
    element: <LoginView></LoginView>,
    title: "登录"
  },
  {
    path: "/register",
    element: <RegisterView></RegisterView>,
    title: "注册"
  },
]

export default RouterList