import axios from "../../utils/axios"
import { message } from 'antd';
export const getLogin = (phone,password) => {
  return dispatch => {
    axios.post("/login", { phone,password}).then((res) => {
      if (res.code === 200) {
        message.open({
          type: 'success',
          content: "登录成功",
        });
        localStorage.setItem("token",JSON.stringify(res.token));
        window.location.reload();
      } else {
        message.open({
          type: 'error',
          content: "手机号密码错误！请重新登录！",
        });
      }
      dispatch({
        type: 'POST_LOGIN',
        payload: res
      })
    })
  }
}