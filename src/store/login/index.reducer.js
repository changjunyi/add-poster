const initState = {
  name: '张三'
}

// reducer函数是一个纯函数 --> 没有任何副作用，副作用就是内部逻辑会影响外部属性或参数
function LoginStore(state = initState, action) {
  switch (action.type) {
    case 'POST_LOGIN':
      return {
        ...state,
        logins: action.payload
      }
    default:
      return state
  }
}

export default LoginStore