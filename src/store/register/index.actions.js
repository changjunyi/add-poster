import axios from "../../utils/axios"
import { message } from 'antd';
export const getRegister = (phone, password) => {
  return dispatch => {
    axios.post("/register", { phone, password }).then((res) => {
      if (res.code === 200) {
        message.open({
          type: 'success',
          content: res.msg,
        });
      } else {
        message.open({
          type: 'error',
          content: "手机号密码错误！请重新注册！",
        });
      }
      dispatch({
        type: 'POST_REGISTER',
        payload: res
      })
    })
  }
}