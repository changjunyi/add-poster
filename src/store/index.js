import {
    createStore,
    applyMiddleware,
    combineReducers
} from 'redux'
import thunk from 'redux-thunk'
import LoginReducer from './login/index.reducer'
import RegisterReducer from "./register/index.reducer"
let bigStore = combineReducers({
    LoginReducer,
    RegisterReducer
})

export default createStore(bigStore, applyMiddleware(thunk))



