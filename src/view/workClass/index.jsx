import React from "react";
import "./style.scss";
import { Layout, Menu, Button } from 'antd';
import {
  DesktopOutlined, FormOutlined, BellOutlined, DeleteOutlined, ReconciliationOutlined
} from '@ant-design/icons';
import { useLocation, useNavigate } from "react-router-dom";
const { Header, Content, Sider } = Layout;
const items = [
  {
    key: "/workbench",
    label: "工作台",
    icon: <DesktopOutlined />,
  },
  {
    key: "/works",
    label: "作品管理",
    icon: <FormOutlined />,
  },
  {
    key: "/material",
    label: "素材管理",
    icon: <ReconciliationOutlined />,
  },
  {
    key: "/recycle",
    label: "回收站",
    icon: <DeleteOutlined />,
  }
];
// 退出登录
const delLogin = () => {
  localStorage.removeItem("token")
  window.location.reload();
}
const Index = ({ children }) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  return (
    <div className="classBox">
      <Layout style={{ width: "100%", height: "100%" }}>
        <Header style={{ background: "#fff" }}>
          <img src="https://lib.eqh5.com/eqx.layout/images/new_logo.svg" />
          <div>
            <span><BellOutlined /></span>&emsp;
            <img src="https://lib.eqh5.com/eqx.layout/images/head_portrait.svg" />
            &emsp;
            <Button type="primary" danger onClick={() => delLogin()}>
              退出登录
            </Button>
          </div>
        </Header>
        <Layout>
          <Sider collapsible width="140px" style={{ background: "#fff" }}>
            <Button style={{width:"90%",marginLeft:"5%",textAlign:"center"}} type="primary" onClick={() => navigate("/")}>
              首页
            </Button>
            <Menu
              mode="inline"
              items={items}
              selectedKeys={[pathname]}
              onClick={({ key }) => navigate(key)}
            ></Menu>
          </Sider>
          <Content style={{ background: "#fff" }}>{children}</Content>
        </Layout>
      </Layout>
    </div>
  )
}

export default Index;