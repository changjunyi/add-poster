import React,{useState} from "react";
import "./style.scss";
import { Layout, Menu, Button } from 'antd';
import {
  PictureOutlined, VideoCameraAddOutlined, DesktopOutlined, LayoutOutlined, FundOutlined, PicLeftOutlined, BellOutlined,
} from '@ant-design/icons';
import { useLocation, useNavigate } from "react-router-dom";
const { Header, Content, Sider } = Layout;
const items = [
  {
    key: "/",
    label: "精选推荐",
    icon: <LayoutOutlined />,
  },
  {
    key: "/workbench",
    label: "工作台",
    icon: <DesktopOutlined />,
  },
  {
    key: "/poster",
    label: "海报",
    icon: <PictureOutlined />
  },
  {
    key: "/form",
    label: "表单",
    icon: <FundOutlined />
  },
  {
    key: "/interaction",
    label: "互动",
    icon: <PicLeftOutlined />,
  },
  {
    key: "/video",
    label: "视频",
    icon: <VideoCameraAddOutlined />
  },
];
// 退出登录
const delLogin = () => {
  localStorage.removeItem("token")
  window.location.reload();
}
// 消息
// const [isHovered, setIsHovered] = useState(false);
// const handleMouseEnter = () => {
//   setIsHovered(true);
//   console.log("111")
// };
// const handleMouseLeave = () => {
//   setIsHovered(false);
//   console.log("222")
// };

const Index = ({ children }) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  return (
    <div className="classBox">
      <Layout style={{ width: "100%", height: "100%" }}>
        <Header style={{ background: "#fff" }}>
          <img src="https://lib.eqh5.com/eqx.layout/images/new_logo.svg" />
          <div>
            <span><BellOutlined /></span>&emsp;
            <img src="https://lib.eqh5.com/eqx.layout/images/head_portrait.svg" />
            &emsp;
            <Button type="primary" danger onClick={() => delLogin()}>
              退出登录
            </Button>
          </div>
        </Header>
        <Layout>
          <Sider collapsible width="140px" style={{ background: "#fff" }}>
            <Menu
              mode="inline"
              items={items}
              selectedKeys={[pathname]}
              onClick={({ key }) => navigate(key)}
            ></Menu>
          </Sider>
          <Content style={{ background: "#fff" ,overflowY:"scroll"}}>{children}</Content>
        </Layout>
      </Layout>
    </div>
  )
}

export default Index;