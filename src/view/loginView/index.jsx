import React, { useState, useEffect } from "react";
import loginImg from "../../assets/login.png"
import "./style.scss"
import { Button, message } from 'antd';
import {
  RightOutlined
} from '@ant-design/icons';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getLogin } from "../../store/login/index.actions";
const Index = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("")
  // 监听输入的值
  const phoneTest = /^1[3456789]\d{9}$/;
  const passTest = /^.{6,16}$/;
  const [isPhone, setIsPhone] = useState(false)
  const [isPass, setIsPass] = useState(false)
  const [phoneText, setPhoneText] = useState("")
  const [phoneColor, setPhoneColor] = useState("red")
  const [passText, setPassText] = useState("")
  const [passColor, setPassColor] = useState("red")
  useEffect(() => {
    if (isPhone) {
      setPhoneText("手机号输入正确")
      setPhoneColor("blue")
    } else {
      if (phone === "") {
        setPhoneText("")
      } else {
        setPhoneText("请输入正确的手机号")
        setPhoneColor("red")
      }
    }
    if (isPass) {
      setPassText("密码输入正确")
      setPassColor("blue")
    } else {
      if (password === "") {
        setPassText("")
      } else {
        setPassText("请输入正确的密码6~16位")
        setPassColor("red")
      }
    }
  }, [phone, isPhone, password, isPass]);
  const phoneChang = (e) => {
    const phoneNumber = e.target.value;
    setPhone(phoneNumber);
    setIsPhone(phoneTest.test(phoneNumber))
  }
  const passChang = (e) => {
    setPassword(e.target.value);
    setIsPass(passTest.test(e.target.value))
  }
  const onLogin = () => {
    if (isPhone && isPass) {
      dispatch(getLogin(phone, password));
    } else {
      message.open({
        type: 'error',
        content: '请输入正确的账号密码！！！',
      });
    }
  }
  return (
    <div className="loginBox">
      <div className="left"><img src={loginImg} /></div>
      <div className="right">
        <h2>手机号登录</h2>
        <div className="inputs">
          <div>
            <input type="text" placeholder="请输入手机号" value={phone} onChange={phoneChang} />
            <span style={{ color: phoneColor, fontSize: "12px" }}>{phoneText}</span>
          </div>
          <div>
            <input type="password" placeholder="请输入密码" value={password} onChange={passChang} />
            <span style={{ color: passColor, fontSize: "12px" }}>{passText}</span>
          </div>
          <Button type="primary" onClick={() => onLogin()}>登录</Button>
        </div>
        <p>注册代表同意<span style={{ color: "blue" }}>《八维A11 Poster平台用户服务协议》</span><span style={{ color: "blue" }} onClick={() => navigate("/register")}>点击前去注册<RightOutlined /></span></p>
      </div>
    </div>
  )
}

export default Index;