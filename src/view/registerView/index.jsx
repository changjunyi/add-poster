import React, { useState, useEffect } from "react";
import loginImg from "../../assets/login.png"
import "./style.scss"
import { Button, message } from 'antd';
import {
  CloseOutlined
} from '@ant-design/icons';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getRegister } from "../../store/register/index.actions"
const Index = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("")
  // 监听输入的值
  const phoneTest = /^1[3456789]\d{9}$/;
  const passTest = /^.{6,16}$/;
  const [isPhone, setIsPhone] = useState(false)
  const [isPass, setIsPass] = useState(false)
  const [phoneText, setPhoneText] = useState("")
  const [phoneColor, setPhoneColor] = useState("red")
  const [passText, setPassText] = useState("")
  const [passColor, setPassColor] = useState("red")
  useEffect(() => {
    if (isPhone) {
      setPhoneText("")
    } else {
      if (phone === "") {
        setPhoneText("")
      } else {
        setPhoneText("请输入正确的手机号")
        setPhoneColor("red")
      }
    }
    if (isPass) {
      setPassText("")
    } else {
      if (password === "") {
        setPassText("")
      } else {
        setPassText("请输入正确的密码6~16位")
        setPassColor("red")
      }
    }
  }, [phone, isPhone, password, isPass]);
  const phoneChang = (e) => {
    const phoneNumber = e.target.value;
    setPhone(phoneNumber);
    setIsPhone(phoneTest.test(phoneNumber))
  }
  const passChang = (e) => {
    setPassword(e.target.value);
    setIsPass(passTest.test(e.target.value))
  }
  const onLogin = () => {
    if (isPhone && isPass) {
      dispatch(getRegister(phone, password));
    } else {
      message.open({
        type: 'error',
        content: '请输入正确的账号密码！！！',
      });
    }
  }
  return (
    <div className="registerBox">
      <div className="left"><img src={loginImg} /></div>
      <div className="right">
        <h2>手机号注册&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<CloseOutlined onClick={()=>navigate(-1)}/></h2>
        <div className="inputs">
          <div>
            <input type="text" placeholder="手机号" value={phone} onChange={phoneChang} />
            <span style={{ color: phoneColor, fontSize: "12px" }}>{phoneText}</span>
          </div>
          <div>
            <input type="password" placeholder="密码(6~16位字符,支持数字、字母,区分大小写)" value={password} onChange={passChang} />
            <span style={{ color: passColor, fontSize: "12px" }}>{passText}</span>
          </div>
          <Button type="primary" onClick={() => onLogin()}>注册</Button>
        </div>
        <p>注册代表同意<span style={{ color: "blue" }}>《八维A11 Poster平台用户服务协议》</span></p>
      </div>
    </div>
  )
}

export default Index;