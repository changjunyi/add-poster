import React from "react";
import ImageSlider from "../../component/imageSlider/index"
import HeaderComponent from "../../component/headerComponent"
import logo from "../../assets/logo2.webp"
const Index = () => {
  return (
    <div><HeaderComponent logo={logo}></HeaderComponent>
    <ImageSlider></ImageSlider></div>
  )
}

export default Index;