import React, { useState } from 'react'
import './style.css'
import one from "../../images/yao.gif"
import two from '../../images/1.png'
import three from '../../images/2.png'
import four from '../../images/3.png'
import five from '../../images/4.png'
const ImageSlider = () => {
  // 状态变量
  const [isHovered, setIsHovered] = useState(false) // 表示容器是否被悬停
  const [isImageHovered, setIsImageHovered] = useState(false) // 表示图片是否被悬停
  const [imageIndex, setImageIndex] = useState(0) // 当前显示图片的索引
  const gifImages = [one] // GIF 图片数组
  const jpgImages = [two, three, four, five] // JPG 图片数组
  const descriptions = ['描述1', '描述2', '描述3', '描述4'] // 图片描述数组

  // 事件处理函数
  const handleMouseEnter = () => {
    setIsHovered(true)
  }

  const handleMouseLeave = () => {
    setIsHovered(false)
    setIsImageHovered(false)
  }

  const handleImageClick = () => {
    setImageIndex((prevIndex) => (prevIndex + 1) % jpgImages.length)
  }

  const handleMouseMove = (event) => {
    const { movementX, target } = event
    const containerWidth = target.clientWidth
    const imagePartWidth = containerWidth / jpgImages.length
    const mouseX = event.clientX - target.getBoundingClientRect().left
    const imageIndex = Math.floor(mouseX / imagePartWidth)
    setImageIndex(imageIndex)
  }

  const handleImageMouseEnter = () => {
    setIsImageHovered(true)
  }

  const handleImageMouseLeave = () => {
    setIsImageHovered(false)
  }

  // 用于显示信息的变量
  const imageCount = jpgImages.length // 图片总数
  const currentImageIndex = imageIndex + 1 // 当前显示图片的索引（从 1 开始）
  const isGIFImage = gifImages.includes(gifImages[0]) // 检查当前图片是否为 GIF 图片
  const progress = (currentImageIndex / imageCount) * 100 // 计算图片幻灯片的进度

  const currentDescription = descriptions[imageIndex] // 当前图片的描述

  return (
    <div className="block">
      <div
        className="block-hoverSwipper"
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={handleImageClick}
        onMouseMove={handleMouseMove}
      >
        <img
          className="img"
          src={isHovered ? jpgImages[imageIndex] : gifImages[0]}
          alt="Image"
          onMouseEnter={handleImageMouseEnter}
          onMouseLeave={handleImageMouseLeave}
        />
        <div className="bar">
          {/* 显示当前图片索引 */}
          {isGIFImage && isImageHovered && (
            <div className="number-bar">
              {`${currentImageIndex}/${imageCount}`}
            </div>
          )}
          <div className="vip">VIP</div>
          {/* 显示 GIF 图片的进度条 */}
          {isGIFImage && isImageHovered && (
            <div
              className="progress-bar"
              style={{ width: `${progress}%` }}
            ></div>
          )}
        </div>
      </div>
      {/* 图片描述动画 */}
      {isImageHovered && (
        <div className="description-animation">{currentDescription}</div>
      )}
      <div className="block-text"></div>
    </div>
  )
}

export default ImageSlider
