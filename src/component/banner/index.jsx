import { Carousel } from 'antd';
 const Banner = ({ carouselRef }) => {
  const imgs = [
    "https://img2.baidu.com/it/u=1236039765,2917177959&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=283",
    "https://img0.baidu.com/it/u=3742240726,3713223915&fm=253&fmt=auto&app=120&f=JPEG?w=607&h=258",
    "https://img2.baidu.com/it/u=1444919735,2637450969&fm=253&fmt=auto&app=138&f=PNG?w=658&h=304",
    "https://img0.baidu.com/it/u=2378265440,2924213767&fm=253&fmt=auto&app=138&f=JPEG?w=900&h=383",
    "https://img0.baidu.com/it/u=1909376831,3343583663&fm=253&fmt=auto&app=138&f=PNG?w=459&h=237",
    "https://img0.baidu.com/it/u=3519616109,3532990776&fm=253&fmt=auto&app=138&f=JPEG?w=658&h=274"
  ];
  return (
    <>
      <Carousel ref={carouselRef} autoplay>
        {
          imgs.map((item, index) => {
            return (
              <img style={{borderRadius:"15px"}} src={item} key={index} alt="" />
            )
          })
        }
      </Carousel>
    </>
  )
}
export default Banner