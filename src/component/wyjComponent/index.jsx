import { useState, useRef } from "react";
import { PlusOutlined, LeftOutlined, RightOutlined, StarFilled } from '@ant-design/icons';
import Banner from "../banner/index"
import db from 'debounce'
import "./index.scss";
 const WyjComponent = () => {
  const [activeTab, setActiveTab] = useState(0);
  const [isHidden, setIsHidden] = useState(true);
  const [offset, setOffset] = useState(0);
  const [offsetTwo, setOffsetTwo] = useState(0);
  const carouselRef = useRef(null);
  const prevOffset = offset - 1;
  const [isShow, setIsShow] = useState(false);
  const list = [
    {
      title: "精选推荐",
      children: [
        {
          title: "111",
          src: "https://img.tuguaishou.com/gif_complete_img/2019-12-04/ad5087fc36a972f395c200a9301c7e75.gif!w350?auth_key=2281968000-0-0-abe3492f3f7fc41e670e5b7dc9739cd6"
        },
        {
          title: "111",
          src: "https://5b0988e595225.cdn.sohucs.com/images/20180123/9c3e8081a94e430c9ef0490320ca0c81.gif"
        },
        {
          title: "111",
          src: "../src/assets/1688437664696-pnackg.webp",
          type: "longPage",
          typeTitle: "长页",
          levelType: "免费"
        },
        {
          title: "111",
          src: "https://file2.rrxh5.cc/g2/c1/2019/04/30/1556614294918.gif"
        },
        {
          title: "111",
          src: "https://img95.699pic.com/photo/40139/7170.gif_wh300.gif"
        },
        {
          title: "111",
          src: "https://file2.rrxh5.cc/g2/c1/2017/04/27/1493282526748.gif"
        },
        {
          title: "111",
          src: "https://p3.itc.cn/images01/20200716/6e695af98bc44c989a2e62bf05c9abbf.gif"
        },
        {
          title: "111",
          src: "https://img2.baidu.com/it/u=2360716971,1911412072&fm=253&fmt=auto&app=138&f=PNG?w=493&h=782"
        },
        {
          title: "111",
          src: "https://img1.baidu.com/it/u=2747388059,380732360&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"
        },
        {
          title: "111",
          src: "https://img0.baidu.com/it/u=2543042260,855792877&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
          more: "查看更多"
        }
      ]
    },
    {
      title: "企业招聘",
      children: [
        {
          title: "222",
          src: "https://img2.baidu.com/it/u=1393667558,1414930650&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=1055"
        },
        {
          title: "222",
          src: "https://img2.baidu.com/it/u=4194058112,3846047378&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=728"
        },
        {
          title: "222",
          src: "https://img1.baidu.com/it/u=2010981555,1917811330&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889"
        },
        {
          title: "222",
          src: "https://img1.baidu.com/it/u=373625511,700021266&fm=253&fmt=auto&app=138&f=JPEG?w=334&h=500"
        },
        {
          title: "222",
          src: "https://img1.baidu.com/it/u=4140075485,3780836671&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750"
        },
        {
          title: "222",
          src: "https://img2.baidu.com/it/u=2197025610,2678209521&fm=253&fmt=auto&app=138&f=JPEG?w=281&h=500"
        },
        {
          title: "222",
          src: "https://img2.baidu.com/it/u=3807078487,1990224181&fm=253&fmt=auto&app=138&f=JPEG?w=334&h=500"
        },
        {
          title: "222",
          src: "https://img1.baidu.com/it/u=4159095655,3841492101&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750"
        },
        {
          title: "222",
          src: "https://img1.baidu.com/it/u=636198146,928151259&fm=253&fmt=auto&app=138&f=JPEG?w=595&h=500"
        },
        {
          title: "222",
          src: "https://img0.baidu.com/it/u=2543042260,855792877&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
          more: "查看更多"
        },
      ]
    },
    {
      title: "培训招生",
      children: [
        {
          title: "333",
          src: "https://img0.baidu.com/it/u=2686545282,925285159&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=690"
        },
        {
          title: "333",
          src: "https://img0.baidu.com/it/u=3791536152,1540154931&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=667"
        },
        {
          title: "333",
          src: "https://img1.baidu.com/it/u=288145693,1010813710&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=667"
        },
        {
          title: "333",
          src: "https://img2.baidu.com/it/u=1387674514,3405097106&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750"
        },
        {
          title: "333",
          src: "https://img1.baidu.com/it/u=1273097896,3217915222&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=753"
        },
        {
          title: "333",
          src: "https://img0.baidu.com/it/u=2103548698,1238702615&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750"
        },
        {
          title: "333",
          src: "https://img2.baidu.com/it/u=4052128366,1323408627&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=1865"
        },
        {
          title: "333",
          src: "https://img1.baidu.com/it/u=1907568965,756474485&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750"
        },
        {
          title: "333",
          src: "https://img1.baidu.com/it/u=1999425870,2421008428&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=890"
        },
        {
          title: "333",
          src: "https://img0.baidu.com/it/u=2543042260,855792877&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
          more: "查看更多"
        },
      ]
    },
    {
      title: "行业标杆玩法",
      children: [
        {
          title: "444",
          src: "https://img1.baidu.com/it/u=3290402872,3160648080&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=777"
        },
        {
          title: "444",
          src: "https://img2.baidu.com/it/u=441022433,2174977136&fm=253&fmt=auto&app=138&f=JPEG?w=348&h=367"
        },
        {
          title: "444",
          src: "https://img2.baidu.com/it/u=4062263051,3849158522&fm=253&fmt=auto&app=120&f=JPEG?w=709&h=921"
        },
        {
          title: "444",
          src: "http://t13.baidu.com/it/u=2534780867,2690936756&fm=224&app=112&f=PNG?w=500&h=500"
        },
        {
          title: "444",
          src: "http://t14.baidu.com/it/u=974028399,292189120&fm=224&app=112&f=JPEG?w=375&h=500"
        },
        {
          title: "444",
          src: "http://t13.baidu.com/it/u=231550021,339848296&fm=224&app=112&f=JPEG?w=500&h=500"
        },
        {
          title: "444",
          src: "https://img0.baidu.com/it/u=242349176,3439426723&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=615"
        },
        {
          title: "444",
          src: "http://t14.baidu.com/it/u=463082737,1809786945&fm=224&app=112&f=JPEG?w=500&h=500"
        },
        {
          title: "444",
          src: "https://img0.baidu.com/it/u=1997171810,118150289&fm=253&fmt=auto&app=138&f=JPG?w=390&h=500"
        },
        {
          title: "444",
          src: "https://img0.baidu.com/it/u=2543042260,855792877&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
          more: "查看更多"
        },
      ]
    }
  ];
  const imageList = [
    "https://img1.baidu.com/it/u=2667103461,1695710211&fm=253&fmt=auto&app=138&f=JPEG?w=617&h=314",
    "https://img2.baidu.com/it/u=58203421,1138713598&fm=253&fmt=auto&app=138&f=JPEG?w=960&h=500",
    "https://img1.baidu.com/it/u=2522788333,2767638807&fm=253&fmt=auto&app=138&f=PNG?w=918&h=452",
    "https://img0.baidu.com/it/u=125953320,2932271806&fm=253&fmt=auto&app=138&f=JPG?w=660&h=408",
    "https://img0.baidu.com/it/u=2321922751,2667725176&fm=253&fmt=auto&app=138&f=PNG?w=500&h=281",
    "https://img0.baidu.com/it/u=18746785,1136665612&fm=253&fmt=auto&app=138&f=PNG?w=658&h=278",
    "https://img2.baidu.com/it/u=2644471119,3666442315&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=282",
    "https://img2.baidu.com/it/u=1226603985,2639734730&fm=253&fmt=auto&app=138&f=JPEG?w=640&h=272",
    "https://img0.baidu.com/it/u=261140024,1033760397&fm=253&fmt=auto&app=138&f=JPG?w=864&h=486",
    "https://img1.baidu.com/it/u=1992078481,1579566954&fm=253&fmt=auto&app=138&f=JPEG?w=801&h=451",
    "https://img1.baidu.com/it/u=2667847558,2707010033&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=200",
    "https://img0.baidu.com/it/u=3308560012,1856867210&fm=253&fmt=auto&app=138&f=JPEG?w=801&h=451"
  ];
  const handleMouseEnter = () => setIsHidden(false);
  const handleMouseLeave = () => setIsHidden(true);
  const showMouseEnter = () => {
    const LongPage = document.querySelector(".long-page");
    const LongPageStar = document.querySelector(".long-page-star");
    const LevelType = document.querySelector(".level-type");
    setIsShow(true);
    LongPage.style.display = "none";
    LongPageStar.style.display = "block";
    LevelType.style.display = "block";
  }
  const showMouseLeave = () => {
    const LongPage = document.querySelector(".long-page");
    const LongPageStar = document.querySelector(".long-page-star");
    const LevelType = document.querySelector(".level-type");
    setIsShow(false);
    LongPage.style.display = "block";
    LongPageStar.style.display = "none";
    LevelType.style.display = "none";
  }
  const handleTabMouseEnter = db((index) => {
    setActiveTab(index);
  }, 400)
  const handlePrevClick = () => carouselRef.current.prev();
  const handleNextClick = () => carouselRef.current.next();
  const handleImagePrev = () => {
    const parentElementWidth = document.querySelector('.image-list').clientWidth;
    setOffset(prevOffset => prevOffset + parentElementWidth);
    document.querySelector('.image-list-item').style.transform = `translateX(${prevOffset + parentElementWidth}px)`;
  }
  const handleImageNext = () => {
    const parentElementWidth = document.querySelector('.image-list').clientWidth;
    setOffset(prevOffset => prevOffset - parentElementWidth);
    document.querySelector('.image-list-item').style.transform = `translateX(${prevOffset - parentElementWidth}px)`;
  }
  const handlePrevList = () => {
    const parentElementWidth = document.querySelector('.page').clientWidth;
    setOffsetTwo(prevOffset => prevOffset + parentElementWidth);
    document.querySelector('.page-item').style.transform = `translateX(${prevOffset + parentElementWidth}px)`;
  }
  const handleNextList = () => {
    const parentElementWidth = document.querySelector('.page').clientWidth;
    setOffsetTwo(prevOffset => prevOffset - parentElementWidth);
    document.querySelector('.page-item').style.transform = `translateX(${prevOffset - parentElementWidth}px)`;
  }
  return (
    <div className="recommend">
      <div onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} className="header">
        <Banner carouselRef={carouselRef}></Banner>
        {!isHidden && (
          <div>
            <LeftOutlined onClick={handlePrevClick} className="left-icon" />
            <RightOutlined onClick={handleNextClick} className="right-icon" />
          </div>
        )}
      </div>
      <div className="image-list">
        <div style={{ transform: `translateX(${offset}px)` }} className="image-list-item">
          {
            imageList.map((item, index) => {
              return (
                <div className="item-image" key={index}>
                  <img src={item} alt="" />
                </div>
              )
            })
          }
        </div>
        <LeftOutlined onClick={handleImagePrev} className="left-icon" />
        <RightOutlined onClick={handleImageNext} className="right-icon" />
      </div>
      <div className="top">
        <div className="title">
          <h2>今日推荐</h2>
        </div>
        <div className="tabs">
          {list.map((item, index) => (
            <span
              key={index}
              className={`tab-title ${index === activeTab ? "active" : ""}`}
              onMouseEnter={() => handleTabMouseEnter(index)}
            >
              {item.title}
            </span>
          ))}
        </div>
        <div className="more">查看更多 &gt;</div>
      </div>
      <div className="bottom">
        <div className="template">
          <div className="blank-creation">
            <PlusOutlined className="creation-icon" />
            <h4>空白创建</h4>
          </div>
          <div className="calendar">
            <img src="https://img1.baidu.com/it/u=540905948,1582468821&fm=253&fmt=auto&app=138&f=JPEG?w=900&h=500" alt="" />
          </div>
        </div>
        <div className="page" >
          <div className="page-item" style={{ transform: `translateX(${offsetTwo}px)` }}>
            {
              list[activeTab].children.map((item, index) => {
                return (
                  <div className="item-list" key={index}>
                    {
                      !(item.more) ? (
                        item.type === "longPage" ? (
                          <div onMouseEnter={showMouseEnter} onMouseLeave={showMouseLeave} style={{ width: "215px", height: "280px", overflow: "hidden", position: "relative" }}>
                            <img style={{ height: "2100px", position: "absolute", transition: `${isShow ? "transform 5s linear" : ""}`, transform: `${isShow ? "translateY(-1820px)" : "translateY(0)"}` }} src={item.src} alt="" />
                            <span className="long-page" style={{ position: "absolute", bottom: "15px", left: "25px", display: "block", width: "50px", height: "25px", background: "rgba(0,0,0,0.7)", textAlign: "center", lineHeight: "25px", color: "#fff", borderRadius: "8px" }}>{item.typeTitle}</span>
                            <StarFilled className="long-page-star" style={{ color: "#fff", background: "rgba(0,0,0,0.7)", fontSize: "16px", width: "25px", height: "25px", textAlign: "center", lineHeight: "25px", position: "absolute", right: "15px", top: "10px", display: "none" }} />
                            <span className="level-type" style={{ color: "#fff", background: "rgba(0,0,0,0.7)", fontSize: "12px", width: "40px", height: "25px", textAlign: "center", lineHeight: "25px", position: "absolute", right: "15px", bottom: "15px", borderRadius: "5px", display: "none" }}>{item.levelType}</span>
                          </div>
                        ) : (
                          <div style={{ width: "215px", height: "280px" }}>
                            <img src={item.src} alt="" />
                          </div>
                        )
                      ) : (
                        <div style={{ width: "215px", height: "280px", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "space-around" }}>
                          <img style={{ width: "100px", height: "100px" }} src={item.src} alt="" />
                          <h4>{item.more}</h4>
                        </div>
                      )
                    }
                  </div>
                )
              })
            }
          </div>
          <LeftOutlined onClick={handlePrevList} className="left-icon" />
          <RightOutlined onClick={handleNextList} className="right-icon" />
        </div>
      </div>
    </div>
  )
}

export default WyjComponent;