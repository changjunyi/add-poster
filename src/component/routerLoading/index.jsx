import React, { useEffect } from "react";
import NProgress from "nprogress";
import "nprogress/nprogress.css";

const RouteLoading = () => {
  useEffect(() => {
    NProgress.start();
    return () => {
      NProgress.done();
    };
  });
  return <div>路由加载中...</div>;
};

export default RouteLoading;