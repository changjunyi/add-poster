import React, { useState } from 'react'
import './style.scss'
import LunarCalendar from 'lunar-calendar'
import BaiduCalendar from 'baidu-calendar'
import { SearchOutlined } from '@ant-design/icons'
// import logo from "../../assets/logo1.webp"
export default function HeaderComponent(props) {
  const [pData, setpData] = useState([
    '全部',
    'h5',
    '海报',
    '长页',
    '表单',
    '互动',
    '电子画册',
  ])
  const [spanValue, setspanValue] = useState(pData[0])
  const today = new Date()
  const year = today.getFullYear()
  const month = today.getMonth() + 1 //注意，getMonth()方法返回的是0-11，因此需要加1来获取实际的月份
  const day = today.getDate()
  // 星期
  const week = today.getDay()
  const weekIF = (week) => {
    let weeks = ''
    switch (week) {
      case 1:
        weeks = '一'
        break
      case 2:
        weeks = '二'
        break
      case 3:
        weeks = '三'
        break
      case 4:
        weeks = '四'
        break
      case 5:
        weeks = '五'
        break
      case 6:
        weeks = '六'
        break
      case 7:
        weeks = '日'
        break
    }
    return weeks
  }
  // 获取今日的阴历日期
  const lunarDate = LunarCalendar.solarToLunar(
    today.getFullYear(),
    today.getMonth() + 1,
    today.getDate()
  )
  const lunarDateString = `${lunarDate.lunarMonthName}${lunarDate.lunarDayName}`
  // 标题改变
  let [index, setIndex] = useState(0)
  const changeSpan = (item, ind) => {
    setspanValue(item)
    setIndex(ind)
  }

  // 状态
  const [keyShow, setkeyShow] = useState('')
  // value
  const [inpValue, setinpValue] = useState('')
  // 搜索
  const inpFocus = () => {
    setkeyShow('none')
  }
  const inpBlur = () => {
    setkeyShow('')
  }
  const onSearch = (e) => {
    setinpValue(e.target.value)
  }
  //设定日期,必填
  let [date, setDate] = useState('2021-2-5')
  //设定年份范围
  let [range, setRange] = useState([2015, 2026])
  //时间监听,必传
  const change = (obj) => {
    // console.log(obj)
  }
  return (
    <div className="top">
      <div className="top-img">
        <img src={props.logo} />
      </div>
      <div className="top-inp">
        <div className="top-all">
          <div className="top-across">
            <span>{spanValue}</span>
            <div className="top-show">
              {pData.length > 0 &&
                pData.map((item, ind) => {
                  return (
                    <p
                      className={index === ind ? 'active' : ''}
                      onClick={() => changeSpan(item, ind)}
                      key={ind}
                    >
                      {item}
                    </p>
                  )
                })}
            </div>
          </div>
          <div className="top-search">
            <input onFocus={inpFocus} onBlur={inpBlur} value={inpValue} onChange={onSearch}/>
            <div className="top-keyframes" style={{ display: keyShow }}>
              {pData.length > 0 &&
                pData.map((item, ind) => {
                  if (ind > 0) {
                    return <p key={ind}>{item}</p>
                  }
                })}
            </div>
          </div>
          <div className="top-icon">
            <SearchOutlined />
          </div>
        </div>
        <div className="top-bot">
          <span>邀请函</span>
          <span>h5</span>
          <span>婚礼邀请函</span>
          <span>招聘</span>
          <span>展会</span>
        </div>
      </div>
      <div className="top-calendar">
        <div className="cal-box">
          <div className="cal-box-top">
            <span className="box-day">{day}</span>/
            <span className="box-month">{month}</span>
            &nbsp;&nbsp;&nbsp;
            <span className="box-year">{year}</span>
          </div>
          <div className="cal-box-bottom">
            周<span>{weekIF(week)}</span>&nbsp; 农历{lunarDateString}
          </div>
        </div>

        <div className="cal-show">
          <div className="app">
            <BaiduCalendar date={date} range={range} change={change} />
          </div>
        </div>
      </div>
    </div>
  )
}
